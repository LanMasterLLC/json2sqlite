package ge.lanmaster.json2sqlite;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public final class Constants {
    public static final String ID = "_id";
    public static final String TYPE = "_type";
    public static final String DEFAULT_FIELD_NAME = "_data";

    public static final Map<String, Types> TYPES;
    public static final Map<Integer, Types> HASH_TYPES;

    static {
        TYPES = new HashMap<>();
        HASH_TYPES = new HashMap<>();
        TYPES.put(JSONObject.class.getName(), Types.JSON_OBJECT);
        HASH_TYPES.put(JSONObject.class.hashCode(), Types.JSON_OBJECT);
        TYPES.put(JSONArray.class.getName(), Types.JSON_ARRAY);
        HASH_TYPES.put(JSONArray.class.hashCode(), Types.JSON_ARRAY);
        TYPES.put(Integer.class.getName(), Types.INTEGER);
        HASH_TYPES.put(Integer.class.hashCode(), Types.INTEGER);
        TYPES.put(Long.class.getName(), Types.LONG);
        HASH_TYPES.put(Long.class.hashCode(), Types.LONG);
        TYPES.put(Double.class.getName(), Types.DOUBLE);
        HASH_TYPES.put(Double.class.hashCode(), Types.DOUBLE);
        TYPES.put(String.class.getName(), Types.STRING);
        HASH_TYPES.put(String.class.hashCode(), Types.STRING);
    }

    public enum Types {
        JSON_OBJECT,
        JSON_ARRAY,
        INTEGER,
        LONG,
        DOUBLE,
        STRING
    }
}
