package ge.lanmaster.json2sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ge.lanmaster.json2sqlite.db.FieldsTableHelper;
import ge.lanmaster.json2sqlite.db.ReferenceTableHelper;
import ge.lanmaster.json2sqlite.db.StoreTable;
import ge.lanmaster.json2sqlite.db.StoreTableHelper;
import ge.lanmaster.json2sqlite.db.TypesTableHelper;
import ge.lanmaster.json2sqlite.pojo.Reference;
import ge.lanmaster.json2sqlite.util.JSONArrayHelper;
import ge.lanmaster.json2sqlite.util.JSONObjectHelper;
import ge.lanmaster.json2sqlite.util.ObjectHelper;

/**
 * Provides methods to Persist, Delete and Retrieve json objects/fields from the database.
 */
public class DBOperationsManager {

    private static Map<Long, String> TYPE_NAMES = new HashMap<>();
    private static Map<Long, String> FIELD_NAMES = new HashMap<>();

    private SQLiteDatabase mDB;

    private ReferenceTableHelper mReferenceHelper;
    private FieldsTableHelper mFieldsHelper;
    private TypesTableHelper mTypesHelper;
    private StoreTableHelper mStoreHelper;

    //(+)Persist

    public DBOperationsManager(SQLiteDatabase pWriteableSQLiteDatabase) {
        mDB = pWriteableSQLiteDatabase;

        mReferenceHelper = new ReferenceTableHelper(mDB);
        mFieldsHelper = new FieldsTableHelper(mDB);
        mTypesHelper = new TypesTableHelper(mDB);
        mStoreHelper = new StoreTableHelper(mDB, mFieldsHelper, mTypesHelper);
    }

    public SQLiteDatabase getDB() {
        return mDB;
    }

    public Reference persistRootObject(Object pObject) {
        long referenceId;
        Reference reference;

        boolean recursionRoot = !mDB.inTransaction();
        if (recursionRoot) {
            mDB.beginTransaction();
        }
        try {
//            Debug.startMethodTracing("json2sqlite_write");

            referenceId = persistNestedObject(pObject, null, null);
            reference = mReferenceHelper.insert(referenceId);

            if (recursionRoot) {
                mDB.setTransactionSuccessful();
            }
        } finally {
            if (recursionRoot) {
                mDB.endTransaction();
            }
//            Debug.stopMethodTracing();
        }

        return reference;
    }

    public Long persistNestedObject(Object pObject, String pFieldName, Long pParentId) {
        boolean recursionRoot = !mDB.inTransaction();
        if (recursionRoot) {
            mDB.beginTransaction();
        }
        Long recordId = null;
        try {
            if (!JSONObject.NULL.equals(pObject))
                switch (Constants.HASH_TYPES.get(pObject.getClass().hashCode())) {
                    case JSON_OBJECT:
                        JSONObject jsonObject = (JSONObject) pObject;
                        recordId = mStoreHelper.create(jsonObject.getClass().getName(), pFieldName, pParentId, null);

                        Iterator<String> keys = jsonObject.keys();
                        while (keys.hasNext()) {
                            String key = keys.next();
                            persistNestedObject(jsonObject.opt(key), key, recordId);
                        }
                        break;
                    case JSON_ARRAY:
                        JSONArray jsonArray = (JSONArray) pObject;
                        recordId = mStoreHelper.create(jsonArray.getClass().getName(), pFieldName, pParentId, null);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            persistNestedObject(jsonArray.opt(i), Integer.toString(i), recordId);
                        }
                        break;
                    case STRING:
                    case LONG:
                    case INTEGER:
                    case DOUBLE:
                        recordId = mStoreHelper.create(pObject.getClass().getName(), pFieldName, pParentId, pObject.toString());
                        break;
                }
            if (recursionRoot) {
                mDB.setTransactionSuccessful();
            }
        } finally {
            if (recursionRoot) {
                mDB.endTransaction();
            }
        }
        return recordId;
    }

    //(-)Persist

    //(+)Retrieve

    public void retrieveRootObject(Reference pReference, ObjectHelper pObjectHelper) {
        //TODO: check params for null / fail if so
        retrieveObject(pReference.getReferencedId(), pObjectHelper);
    }

    public void retrieveObject(Long pID, ObjectHelper pObjectHelper) {
        boolean recursionRoot = !mDB.inTransaction();
        if (recursionRoot) {
            mDB.beginTransaction();
//            Debug.startMethodTracing("json2sqlite_read");
        }

        try {
            Long id = null, fieldId = null, typeId = null;
            String value = null;

            Cursor cursor = mStoreHelper.getRowById(pID);
            if (cursor.moveToFirst()) {
                /**
                 * @see StoreTable.getRowById(SQLiteDatabase pDB, Long pId)
                 */
                id = cursor.getLong(0);
                fieldId = cursor.getLong(1);
                typeId = cursor.getLong(2);
                value = cursor.getString(3);
            }
            cursor.close();

            String fieldNameString = FIELD_NAMES.get(fieldId);
            if (fieldNameString == null) {
                fieldNameString = mFieldsHelper.getNameById(fieldId);
                FIELD_NAMES.put(fieldId, fieldNameString);
            }
            if (fieldNameString == null || fieldNameString.isEmpty()) {
                fieldNameString = Constants.DEFAULT_FIELD_NAME;
            }

            //determine referenced object type
            String typeString = TYPE_NAMES.get(typeId);
            if (typeString == null) {
                typeString = mTypesHelper.getNameById(typeId);
                TYPE_NAMES.put(typeId, typeString);
            }
            switch (Constants.TYPES.get(typeString)) {
                case JSON_OBJECT:
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(Constants.ID, id);
                    pObjectHelper.put(fieldNameString, jsonObject);
                    JSONObjectHelper objectHelper = new JSONObjectHelper(jsonObject);

                    //PERFORMANCE TRICK:
                    //LOAD ARRAY ITEMS OF JSONObject TYPE ONLY IF IT IS A DIRECT CALL TO THIS METHOD
                    if (pObjectHelper instanceof JSONArrayHelper) {
                        if (!recursionRoot) break;
                    }
                    retrieveNestedObjects(objectHelper, id);
                    break;
                case JSON_ARRAY:
                    JSONArray jsonArray = new JSONArray();
                    pObjectHelper.put(fieldNameString, jsonArray);
                    JSONArrayHelper arrayHelper = new JSONArrayHelper(jsonArray);
                    retrieveNestedObjects(arrayHelper, id);
                    break;
                case STRING:
                    pObjectHelper.put(fieldNameString, value);
                    break;
                case LONG:
                    pObjectHelper.put(fieldNameString, Long.parseLong(value));
                    break;
                case INTEGER:
                    pObjectHelper.put(fieldNameString, Integer.parseInt(value));
                    break;
                case DOUBLE:
                    pObjectHelper.put(fieldNameString, Double.parseDouble(value));
                    break;
            }

            if (recursionRoot) {
                mDB.setTransactionSuccessful();
            }
        } catch (JSONException e) {
            Log.e("TAG", "MSG", e);
        } finally {
            if (recursionRoot) {
                mDB.endTransaction();
//                Debug.stopMethodTracing();
            }
        }
    }

    private void retrieveNestedObjects(ObjectHelper pObjectHelper, Long pId) throws JSONException {
        Cursor cursor = mStoreHelper.getIDsByParentId(pId);
        try {
            while (cursor.moveToNext()) {
                Long id = cursor.getLong(0);
                retrieveObject(id, pObjectHelper);
            }
        } finally {
            cursor.close();
        }
    }

    //(-)Retrieve

    //(+)Delete

    public void deleteRootObject(Reference pReference) {
        mReferenceHelper.delete(pReference.getId());

        //cleanup or recursive remove ID
        deleteObject(pReference.getReferencedId());
    }

    public void deleteObject(Long pID) {
        boolean recursionRoot = !mDB.inTransaction();
        if (recursionRoot) {
            mDB.beginTransaction();
//            Debug.startMethodTracing("json2sqlite_delete");
        }

        try {
            if (mStoreHelper.delete(pID) > 0) {
                Cursor children = mStoreHelper.getIDsByParentId(pID);
                while (children.moveToNext()) {
                    Long id = children.getLong(0);
                    deleteObject(id);
                }
                children.close();
            }
            if (recursionRoot) {
                mDB.setTransactionSuccessful();
            }
        } finally {
            if (recursionRoot) {
                mDB.endTransaction();
//                Debug.stopMethodTracing();
            }
        }
    }

    public void cleanup() {
        //delete objects which have no parents or references
        //delete unused field names
        //delete unused types
    }

    public Long deleteAllWithType(String pTypeName) {
        return 0l;
    }

    //(-)Delete
}
