package ge.lanmaster.json2sqlite.util;

import org.json.JSONArray;
import org.json.JSONException;

import ge.lanmaster.json2sqlite.util.ObjectHelper;

public class JSONArrayHelper implements ObjectHelper<JSONArray, Integer> {

    private JSONArray mArray;

    public JSONArrayHelper(JSONArray pArray) {
        mArray = pArray;
    }

    @Override
    public JSONArray getWrappedObject() {
        return mArray;
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(String key, Boolean value) throws JSONException {
        return put(Integer.valueOf(key), value);
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(Integer key, Boolean value) throws JSONException {
        mArray.put(key.intValue(), value);
        return this;
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(String key, Double value) throws JSONException {
        return put(Integer.valueOf(key), value);
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(Integer key, Double value) throws JSONException {
        mArray.put(key.intValue(), value);
        return this;
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(String key, Integer value) throws JSONException {
        return put(Integer.valueOf(key), value);
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(Integer key, Integer value) throws JSONException {
        mArray.put(key.intValue(), value);
        return this;
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(String key, Long value) throws JSONException {
        return put(Integer.valueOf(key), value);
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(Integer key, Long value) throws JSONException {
        mArray.put(key.intValue(), value);
        return this;
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(String key, Object value) throws JSONException {
        return put(Integer.valueOf(key), value);
    }

    @Override
    public ObjectHelper<JSONArray, Integer> put(Integer key, Object value) throws JSONException {
        mArray.put(key.intValue(), value);
        return this;
    }
}
