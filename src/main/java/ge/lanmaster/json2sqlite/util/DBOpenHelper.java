package ge.lanmaster.json2sqlite.util;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ge.lanmaster.json2sqlite.db.FieldsTable;
import ge.lanmaster.json2sqlite.db.ReferenceTable;
import ge.lanmaster.json2sqlite.db.StoreTable;
import ge.lanmaster.json2sqlite.db.TypesTable;

public class DBOpenHelper extends SQLiteOpenHelper {

    public static final int VERSION = 1;

    public DBOpenHelper(Context pContext, String pDatabaseName) {
        this(pContext, pDatabaseName, null);
    }

    public DBOpenHelper(Context pContext, String pDatabaseName, SQLiteDatabase.CursorFactory pCursorFactory) {
        this(pContext, pDatabaseName, pCursorFactory, null);
    }

    public DBOpenHelper(Context pContext, String pName, SQLiteDatabase.CursorFactory pCursorFactory, DatabaseErrorHandler pDatabaseErrorHandler) {
        super(pContext, pName, pCursorFactory, VERSION, pDatabaseErrorHandler);
        getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        TypesTable.onCreate(db);
        FieldsTable.onCreate(db);
        StoreTable.onCreate(db);
        ReferenceTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        TypesTable.onUpgrade(db, oldVersion, newVersion);
        FieldsTable.onUpgrade(db, oldVersion, newVersion);
        StoreTable.onUpgrade(db, oldVersion, newVersion);
        ReferenceTable.onUpgrade(db, oldVersion, newVersion);
    }

}
