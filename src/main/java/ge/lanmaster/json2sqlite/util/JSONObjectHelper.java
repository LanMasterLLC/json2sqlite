package ge.lanmaster.json2sqlite.util;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONObjectHelper implements ObjectHelper<JSONObject, String> {

    private JSONObject mObject;

    public JSONObjectHelper(JSONObject pObject) {
        mObject = pObject;
    }

    @Override
    public JSONObject getWrappedObject() {
        return mObject;
    }

    @Override
    public ObjectHelper<JSONObject, String> put(String key, Boolean value) throws JSONException {
        mObject.put(key, value);
        return this;
    }

    @Override
    public ObjectHelper<JSONObject, String> put(String key, Double value) throws JSONException {
        mObject.put(key, value);
        return this;
    }

    @Override
    public ObjectHelper<JSONObject, String> put(String key, Integer value) throws JSONException {
        mObject.put(key, value);
        return this;
    }

    @Override
    public ObjectHelper<JSONObject, String> put(String key, Long value) throws JSONException {
        mObject.put(key, value);
        return this;
    }

    @Override
    public ObjectHelper<JSONObject, String> put(String key, Object value) throws JSONException {
        mObject.put(key, value);
        return this;
    }
}
