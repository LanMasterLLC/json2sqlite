package ge.lanmaster.json2sqlite.util;

import org.json.JSONException;

public interface ObjectHelper<T, K> {

    T getWrappedObject();

    ObjectHelper<T, K> put(String key, Boolean value) throws JSONException;

    ObjectHelper<T, K> put(K key, Boolean value) throws JSONException;

    ObjectHelper<T, K> put(String key, Double value) throws JSONException;

    ObjectHelper<T, K> put(K key, Double value) throws JSONException;

    ObjectHelper<T, K> put(String key, Integer value) throws JSONException;

    ObjectHelper<T, K> put(K key, Integer value) throws JSONException;

    ObjectHelper<T, K> put(String key, Long value) throws JSONException;

    ObjectHelper<T, K> put(K key, Long value) throws JSONException;

    ObjectHelper<T, K> put(String key, Object value) throws JSONException;

    ObjectHelper<T, K> put(K key, Object value) throws JSONException;
}
