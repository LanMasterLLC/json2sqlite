package ge.lanmaster.json2sqlite.pojo;

public class Reference {
    private long mId, mReferencedId;

    public Reference() {
    }

    public Reference(long pId, long pReferencedId) {
        mId = pId;
        mReferencedId = pReferencedId;
    }

    public long getId() {
        return mId;
    }

    public void setId(long pId) {
        mId = pId;
    }

    public long getReferencedId() {
        return mReferencedId;
    }

    public void setReferencedId(long pReferencedId) {
        mReferencedId = pReferencedId;
    }
}
