package ge.lanmaster.json2sqlite.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class StoreTableHelper {

    private static final String getIDByRowIdStatement = "SELECT " + StoreTableContract._ID + " FROM " + StoreTableContract.TABLE_NAME + " WHERE rowid=?";
    private static final String insertStatement = "INSERT INTO " + StoreTableContract.TABLE_NAME + "(" + StoreTableContract._PARENT_ID + "," + StoreTableContract._FIELD_ID + "," + StoreTableContract._TYPE_ID + "," + StoreTableContract._VALUE + ")" + "VALUES(?,?,?,?)";
    private static final String deleteStatement = "DELETE FROM " + StoreTableContract.TABLE_NAME + " WHERE " + StoreTableContract._ID + "=?";

    private static final String getIDsByParentIdQuery = "SELECT " + StoreTableContract._ID + " FROM " + StoreTableContract.TABLE_NAME + " WHERE " + StoreTableContract._PARENT_ID + "=?";
    public static final String getRowByIdQuery = "SELECT " + StoreTableContract._ID + "," + StoreTableContract._FIELD_ID + "," + StoreTableContract._TYPE_ID + "," + StoreTableContract._VALUE + " FROM " + StoreTableContract.TABLE_NAME + " WHERE " + StoreTableContract._ID + "=?";

    private SQLiteStatement compiledGetIdByRowIdStatement;
    private SQLiteStatement compiledInsertStatement;
    private SQLiteStatement compiledDeleteStatement;

    private FieldsTableHelper mFieldsHelper;
    private TypesTableHelper mTypesHelper;

    private SQLiteDatabase mDB;

    public StoreTableHelper(SQLiteDatabase pDB, FieldsTableHelper pFieldsHelper, TypesTableHelper pTypesHelper) {
        mFieldsHelper = pFieldsHelper;
        mTypesHelper = pTypesHelper;

        compiledGetIdByRowIdStatement = pDB.compileStatement(getIDByRowIdStatement);
        compiledInsertStatement = pDB.compileStatement(insertStatement);
        compiledDeleteStatement = pDB.compileStatement(deleteStatement);

        mDB = pDB;
    }

    public Long create(String type, String fieldName, Long parentId, String value) {
        Long typeId = mTypesHelper.getOrCreateIdForName(type);

        fieldName = fieldName == null ? new String() : fieldName;
        Long fieldId = mFieldsHelper.getOrCreateIdForName(fieldName);

        return create(typeId, fieldId, parentId, value);
    }

    private Long create(Long typeId, Long fieldId, Long parentId, String value) {
        if (parentId == null) {
            parentId = 0L;
        }

        if (value == null) {
            value = new String();
        }

        compiledInsertStatement.bindLong(1, parentId);
        compiledInsertStatement.bindLong(2, fieldId);
        compiledInsertStatement.bindLong(3, typeId);
        compiledInsertStatement.bindString(4, value);

        long rowId = compiledInsertStatement.executeInsert();

        compiledGetIdByRowIdStatement.bindLong(1, rowId);

        return compiledGetIdByRowIdStatement.simpleQueryForLong();
    }

    public Integer delete(Long pID) {
        compiledDeleteStatement.bindLong(1, pID);
        return compiledDeleteStatement.executeUpdateDelete();
    }

    public Cursor getRowById(Long pId) {
        return mDB.rawQuery(getRowByIdQuery, new String[]{Long.toString(pId)});
    }

    public Cursor getIDsByParentId(Long pParentId) {
        return mDB.rawQuery(getIDsByParentIdQuery, new String[]{Long.toString(pParentId)});
    }

}
