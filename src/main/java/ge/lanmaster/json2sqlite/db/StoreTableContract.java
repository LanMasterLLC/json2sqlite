package ge.lanmaster.json2sqlite.db;

import android.provider.BaseColumns;

public class StoreTableContract implements BaseColumns {
    public static final String TABLE_NAME = "store";

    public static final String _PARENT_ID = "_parent_id";
    public static final String _FIELD_ID = "_field_id";
    public static final String _TYPE_ID = "_type_id";
    public static final String _VALUE = "_value";

    public static final String[] PROJECTION_ALL = new String[]{_ID, _PARENT_ID, _FIELD_ID, _TYPE_ID, _VALUE};
}
