package ge.lanmaster.json2sqlite.db;

import android.database.sqlite.SQLiteDatabase;

public class TypesTable {
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TypesTableContract.TABLE_NAME + " ( " +
                    TypesTableContract._ID + " INTEGER, " +
                    TypesTableContract._NAME + " TEXT, " +
                    "PRIMARY KEY ( " + TypesTableContract._ID + " )" +
                    " ); ";

    public static void onCreate(SQLiteDatabase pDB) {
        pDB.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase pDB, int pOldVersion, int pNewVersion) {
        pDB.execSQL("DROP TABLE IF EXISTS " + TypesTableContract.TABLE_NAME + ";");
        onCreate(pDB);
    }

}
