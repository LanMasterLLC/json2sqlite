package ge.lanmaster.json2sqlite.db;

import android.database.sqlite.SQLiteDatabase;

public class FieldsTable {
    private static final String CREATE_TABLE =
            "CREATE TABLE " + FieldsTableContract.TABLE_NAME + " ( " +
                    FieldsTableContract._ID + " INTEGER, " +
                    FieldsTableContract._NAME + " TEXT, " +
                    "PRIMARY KEY ( " + FieldsTableContract._ID + " )" +
                    " ); ";

    public static void onCreate(SQLiteDatabase pDB) {
        pDB.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase pDB, int pOldVersion, int pNewVersion) {
        pDB.execSQL("DROP TABLE IF EXISTS " + FieldsTableContract.TABLE_NAME + ";");
        onCreate(pDB);
    }
}
