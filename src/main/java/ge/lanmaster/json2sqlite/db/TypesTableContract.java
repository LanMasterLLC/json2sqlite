package ge.lanmaster.json2sqlite.db;

import android.provider.BaseColumns;

public class TypesTableContract implements BaseColumns {
    public static final String TABLE_NAME = "types";

    public static final String _NAME = "_name";

    public static final String[] PROJECTION_ALL = new String[]{_ID, _NAME};
}
