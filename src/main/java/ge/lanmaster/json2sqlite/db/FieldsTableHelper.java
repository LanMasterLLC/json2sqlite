package ge.lanmaster.json2sqlite.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.HashMap;
import java.util.Map;

public class FieldsTableHelper {

    private static final String insertStatement = "INSERT INTO " + FieldsTableContract.TABLE_NAME + " (" + FieldsTableContract._NAME + ") VALUES (?)";
    private static final String queryIdStatement = "SELECT " + FieldsTableContract._ID + " FROM " + FieldsTableContract.TABLE_NAME + " WHERE " + FieldsTableContract._NAME + "=?";
    private static final String queryNameStatement = "SELECT " + FieldsTableContract._NAME + " FROM " + FieldsTableContract.TABLE_NAME + " WHERE " + FieldsTableContract._ID + "=?";

    private SQLiteStatement compiledInsertStatement;
    private SQLiteStatement compiledQueryIdStatement;
    private SQLiteStatement compiledQueryNameStatement;

    private Map<String, Long> mCache;

    public FieldsTableHelper(SQLiteDatabase pDB) {
        compiledInsertStatement = pDB.compileStatement(insertStatement);
        compiledQueryIdStatement = pDB.compileStatement(queryIdStatement);
        compiledQueryNameStatement = pDB.compileStatement(queryNameStatement);

        mCache = new HashMap<>();
    }

    public Long insert(String pName) {
        compiledInsertStatement.bindString(1, pName);
        compiledInsertStatement.executeInsert();
        return getIdByName(pName);
    }

    public Long getOrCreateIdForName(String pName) {
        Long result = mCache.get(pName);
        if (result == null) {
            result = getIdByName(pName);
            if (result == null) {
                result = insert(pName);
            }
            mCache.put(pName, result);
        }
        return result;
    }

    public Long getIdByName(String pName) {
        Long result = null;
        compiledQueryIdStatement.bindString(1, pName);
        try {
            result = compiledQueryIdStatement.simpleQueryForLong();
        } finally {
            return result;
        }
    }

    public String getNameById(Long pID) {
        compiledQueryNameStatement.bindLong(1, pID);
        return compiledQueryNameStatement.simpleQueryForString();
    }
}
