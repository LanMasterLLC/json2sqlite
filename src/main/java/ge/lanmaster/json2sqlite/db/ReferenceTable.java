package ge.lanmaster.json2sqlite.db;

import android.database.sqlite.SQLiteDatabase;

public class ReferenceTable {
    public static final String CREATE_TABLE =
            "CREATE TABLE " + ReferenceTableContract.TABLE_NAME + " ( " +
                    ReferenceTableContract._ID + " INTEGER, " +
                    ReferenceTableContract._REFERENCED_ID + " INTEGER, " +
                    "PRIMARY KEY ( " + ReferenceTableContract._ID + " ) " +
                    " ); ";

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ReferenceTableContract.TABLE_NAME + ";");
        onCreate(db);
    }
}
