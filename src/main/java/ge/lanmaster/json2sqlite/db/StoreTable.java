package ge.lanmaster.json2sqlite.db;

import android.database.sqlite.SQLiteDatabase;

public class StoreTable {
    public static final String CREATE_TABLE =
            "CREATE TABLE " + StoreTableContract.TABLE_NAME + " ( " +
                    StoreTableContract._ID + " INTEGER, " +
                    StoreTableContract._PARENT_ID + " INTEGER NOT NULL, " +
                    StoreTableContract._FIELD_ID + " INTEGER NOT NULL, " +
                    StoreTableContract._TYPE_ID + " INTEGER NOT NULL, " +
                    StoreTableContract._VALUE + " TEXT NOT NULL, " +
                    "PRIMARY KEY ( " + StoreTableContract._ID + " ), " +
                    "FOREIGN KEY ( " + StoreTableContract._TYPE_ID + " ) REFERENCES " + TypesTableContract.TABLE_NAME + " ( " + TypesTableContract._ID + " ), " +
                    "FOREIGN KEY ( " + StoreTableContract._FIELD_ID + " ) REFERENCES " + FieldsTableContract.TABLE_NAME + " ( " + FieldsTableContract._ID + " ) " +
                    " ); ";

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + StoreTableContract.TABLE_NAME + ";");
        onCreate(db);
    }
}
