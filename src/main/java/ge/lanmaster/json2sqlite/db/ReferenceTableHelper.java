package ge.lanmaster.json2sqlite.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import ge.lanmaster.json2sqlite.pojo.Reference;

public class ReferenceTableHelper {

    private static final String insertStatement = "INSERT INTO " + ReferenceTableContract.TABLE_NAME + " (" + ReferenceTableContract._REFERENCED_ID + ") VALUES (?)";
    private static final String queryIdStatement = "SELECT " + ReferenceTableContract._ID + " FROM " + ReferenceTableContract.TABLE_NAME + " WHERE " + ReferenceTableContract._REFERENCED_ID + "=?";
    private static final String queryReferencedIdStatement = "SELECT " + ReferenceTableContract._REFERENCED_ID + " FROM " + ReferenceTableContract.TABLE_NAME + " WHERE " + ReferenceTableContract._ID + "=?";
    private static final String deleteStatement = "DELETE FROM " + ReferenceTableContract.TABLE_NAME + " WHERE " + ReferenceTableContract._ID + "=?";

    private SQLiteStatement compiledInsertStatement;
    private SQLiteStatement compiledQueryIdStatement;
    private SQLiteStatement compiledQueryReferencedIdStatement;
    private SQLiteStatement compiledDeleteStatement;

    public ReferenceTableHelper(SQLiteDatabase pDB) {
        compiledInsertStatement = pDB.compileStatement(insertStatement);
        compiledQueryIdStatement = pDB.compileStatement(queryIdStatement);
        compiledQueryReferencedIdStatement = pDB.compileStatement(queryReferencedIdStatement);
        compiledDeleteStatement = pDB.compileStatement(deleteStatement);
    }

    public Reference insert(Long referencedId) {
        Reference result = new Reference();
        result.setReferencedId(referencedId);

        compiledInsertStatement.bindLong(1, referencedId);
        if (compiledInsertStatement.executeInsert() > 0) {
            result.setId(getIdForReferencedId(referencedId));
        }

        return result;
    }

    public Long getIdForReferencedId(Long referencedId) {
        compiledQueryIdStatement.bindLong(1, referencedId);
        return compiledQueryIdStatement.simpleQueryForLong();
    }

    public Long getReferencedIdForId(Long referencedId) {
        compiledQueryReferencedIdStatement.bindLong(1, referencedId);
        return compiledQueryReferencedIdStatement.simpleQueryForLong();
    }

    public int delete(Long pId) {
        compiledDeleteStatement.bindLong(1, pId);
        return compiledDeleteStatement.executeUpdateDelete();
    }
}
