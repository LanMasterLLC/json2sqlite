package ge.lanmaster.json2sqlite.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.HashMap;
import java.util.Map;

public class TypesTableHelper {

    private static final String insertStatement = "INSERT INTO " + TypesTableContract.TABLE_NAME + " (" + TypesTableContract._NAME + ") VALUES (?)";
    private static final String queryIdStatement = "SELECT " + TypesTableContract._ID + " FROM " + TypesTableContract.TABLE_NAME + " WHERE " + TypesTableContract._NAME + "=?";
    private static final String queryNameStatement = "SELECT " + TypesTableContract._NAME + " FROM " + TypesTableContract.TABLE_NAME + " WHERE " + TypesTableContract._ID + "=?";

    private SQLiteStatement compiledInsertStatement;
    private SQLiteStatement compiledQueryIdStatement;
    private SQLiteStatement compiledQueryNameStatement;

    private Map<String, Long> mCache;

    public TypesTableHelper(SQLiteDatabase pDB) {
        compiledInsertStatement = pDB.compileStatement(insertStatement);
        compiledQueryIdStatement = pDB.compileStatement(queryIdStatement);
        compiledQueryNameStatement = pDB.compileStatement(queryNameStatement);

        mCache = new HashMap<>();
    }

    public Long insert(String pName) {
        compiledInsertStatement.bindString(1, pName);
        compiledInsertStatement.executeInsert();
        return getIdByName(pName);
    }


    public Long getOrCreateIdForName(String type) {
        Long result = mCache.get(type);
        if (result == null) {
            result = getIdByName(type);
            if (result == null) {
                result = insert(type);
            }
            mCache.put(type, result);
        }
        return result;
    }

    public Long getIdByName(String pName) {
        Long result = null;
        compiledQueryIdStatement.bindString(1, pName);
        try {
            result = compiledQueryIdStatement.simpleQueryForLong();
        } finally {
            return result;
        }
    }

    public String getNameById(Long pID) {
        compiledQueryNameStatement.bindLong(1, pID);
        return compiledQueryNameStatement.simpleQueryForString();
    }

}
