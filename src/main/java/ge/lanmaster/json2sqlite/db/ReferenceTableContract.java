package ge.lanmaster.json2sqlite.db;

import android.provider.BaseColumns;

public class ReferenceTableContract implements BaseColumns {
    public static final String TABLE_NAME = "reference";

    public static final String _REFERENCED_ID = "_referenced_id";

    public static final String[] PROJECTION_ALL = new String[]{_ID, _REFERENCED_ID};
}
